# UNPREDICTABLE


**unpredictable***
>  
*  ``*
adjective

B2 likely to change suddenly and without reason and therefore not able to be predicted (= expected before it happens) or depended on:
The weather there can be unpredictable - one minute it's blue skies and the next minute it's pouring rain.
The hours in this job are very unpredictable - you sometimes have to work late at very short notice.
    She's very unpredictable so there's no knowing how she'll react to the news.
    Unfortunately the outcome of this kind of treatment can be rather unpredictable.
    He's fun to be with but can be a bit unpredictable at times, you never know what kind of mood he's going to be in.
    I really enjoy having a holiday in the UK, if only the weather weren't so unpredictable.
    The finishing time is a bit unpredictable, it depends how things go.
